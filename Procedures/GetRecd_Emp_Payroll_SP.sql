DROP PROCEDURE IF EXISTS Emp_Payroll;
delimiter $$
CREATE PROCEDURE Emp_Payroll()
	BEGIN
		DECLARE TARGET_ID int; DECLARE err VARCHAR(128);
		DECLARE	TARGET_WAGE decimal(15,2);
		DECLARE n INT DEFAULT 0;
		DECLARE i INT DEFAULT 1;
		SET n = (SELECT COUNT(*) FROM Employees) + 1;
		SET i = 1;
    
		WHILE i <= n DO
			SET TARGET_ID = i;
			SET TARGET_WAGE = (SELECT EMP_WAGE FROM Employees WHERE EMP_ID = i);
            
            IF EXISTS (SELECT * FROM Employees WHERE EMP_ID = i) THEN BEGIN
				INSERT INTO Payroll (EMP_ID, PAY_DATE, PAY_WAGE) 
							VALUES (i, CURDATE(), TARGET_WAGE);
			END;
            END IF;
            SET i = i + 1;
		END WHILE;
    END;
$$
