DROP PROCEDURE IF EXISTS STATUS_STOCKED;

DELIMITER $$
CREATE PROCEDURE STATUS_STOCKED ()
BEGIN
DECLARE S_OID INT;
IF EXISTS (SELECT ORDER_ID FROM Order_Invoice WHERE ORDER_STATUS = 'received') THEN
	SET S_OID = (SELECT DISTINCT ORDER_ID FROM Order_Invoice WHERE ORDER_STATUS LIKE 'received');
	UPDATE Order_Invoice
    SET ORDER_STATUS = 'stocked'
    WHERE ORDER_ID = S_OID;
END IF;
END;
$$
