DROP TRIGGER checkAge;

DELIMITER $$
CREATE TRIGGER checkAge BEFORE INSERT ON Employees
FOR EACH ROW
	BEGIN
		DECLARE baddata INT;
        DECLARE err VARCHAR(128);
		SET baddata = 0;
		IF NEW.EMP_AGE < 16 THEN
			SET baddata = 1;
		END IF;
    
		IF NEW.EMP_AGE > 100 THEN
			SET baddata = 1;
		END IF;
    
		IF baddata = 1 THEN
			SET err = CONCAT("cannot insert, invalid age", NEW.EMP_AGE, "is invalid");
            signal sqlstate '45000' set message_text = err;
		END IF;
	END;
    
CREATE TRIGGER checkAge BEFORE UPDATE ON Employees
FOR EACH ROW
	BEGIN
		DECLARE baddata INT;
        DECLARE err VARCHAR(128);
		SET baddata = 0;
		IF NEW.EMP_AGE < 16 THEN
			SET baddata = 1;
		END IF;
    
		IF NEW.EMP_AGE > 100 THEN
			SET baddata = 1;
		END IF;
    
		IF baddata = 1 THEN
			SET err = CONCAT("cannot insert, invalid age", NEW.EMP_AGE, "is invalid");
            signal sqlstate '45000' set message_text = err;
		END IF;
	END;$$
    $$
    
    
    